// Importations React
import React from 'react';

// Importation du css
import '../css/Home.css';

// Importations des components
import Object from '../components/Object';

// Autres importations
import { possessionsApiService } from '../services/possessionsApiService';

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            objets: [
                { image: 'test', nom: 'Test Image', marque: 'Marque objet', categorie: 'Catégorie objet' },
                { image: 'test2', nom: 'Test Image 2', marque: 'Marque objet', categorie: 'Catégorie objet' },
                { nom: 'Test Image 3', marque: 'Marque objet', categorie: 'Catégorie objet' }
            ]
        }
    }

    render() {
        return (
            <div className='divHome'>
                <h1 className='titreHome'>Bienvenue !</h1>

                <h2 className='titreHome'>Catégories</h2>

                <h3>Derniers objets ajoutés par nos utilisateurs...</h3>
                
                <div className='rowObjects'>
                    {this.state.objets.length > 0 ?
                        this.state.objets.map((objet, index) =>
                            <Object 
                                key={index}
                                objet={objet}
                            />
                        )
                        :
                        <p>Aucun objet ajouté.</p>
                    }
                </div>
            </div>
        );
    }
}