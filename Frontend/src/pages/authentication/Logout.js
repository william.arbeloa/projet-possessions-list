// Importations React
import React from 'react';

// Importation du css
import '../../css/Authentication.css';

// Autres importations
import { possessionsApiService } from '../../services/possessionsApiService';

export default class Logout extends React.Component {
    async componentDidMount() {
        // Fonction pour se déconnecter
        try {
            let logout = possessionsApiService.request({
                url: '/logout',
                method: 'GET'
            });

            if (logout.data.result) {
                sessionStorage.clear();
                window.location.reload(true);
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <div className='divAuth'>
                <h1 className='titreAuth'>Déconnexion</h1>

                <p className='titreAuth'>Vous êtes maintenant déconnecté.</p>
            </div>
        );
    }
}