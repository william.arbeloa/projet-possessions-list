# Liste de Possessions - FrontEnd

## Installation
Taper `cd /Frontend` pour se rendre dans le dossier Frontend.
Puis taper `npm install` pour installer les node_modules.

## Lancement
Taper `npm start` pour démarrer le FrontEnd. Il est possible qu'il demande à se lancer sur un autre port, car le port 3000 sera utilisé par l'API.

## Connexion
Après que l'API ait été démarrée pour la première fois, deux utilisateurs (Administrateur et Utilisateur) sont créés :

`Admin`
email : admin@test.fr
mdp : test

`Utilisateur`
email : user@test.fr
mdp : test

## Attention
Il est possible que le back et le front se connectent sur le même port, dans ce cas il faudra modifier la configuration pour qu'ils aient un port différent.