const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const models = require('./index');

let modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    image: {
        type: Sequelize.DataTypes.BLOB,
        allowNull: true
    },
    libelle: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: {
                msg: "Le libellé de l'article ne peut pas être vide."
            },
            notEmpty: {
                msg: "Le libellé doit être renseigné."
            },
            len: {
                args: [1, 50],
                msg: "Le libellé doit faire entre 1 et 50 caractères."
            }
        }
    },
    description: {
        type: Sequelize.DataTypes.TEXT,
        allowNull: true
    }
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let articlesModel = db.define('articles', modelDefinition, modelOptions);

module.exports = articlesModel;