const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const models = require('./index');

const modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    libelle: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        validate: {
            len: {
                args: [1, 250],
                msg: "Le nom de la catégorie doit faire entre 1 et 250 caractères."
            },
            notNull: {
                msg: "L'article a forcément une catégorie."
            },
            notEmpty: {
                msg: "La catégorie de l'article doit être renseignée."
            }
        }
    }
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let categoriesModel = db.define('categories', modelDefinition, modelOptions);

categoriesModel.hasMany(models.articles);

module.exports = categoriesModel;