# Liste de Possessions - API

## Fichier d'environnement dev
Créer un fichier .env à la racine du dossier API avec le contenu ci-dessous.
```
DATABASE_HOST=
DATABASE_USERNAME=
DATABASE_NAME='PossessionsList'
DATABASE_PASSWORD=
DATABASE_PORT=5432
PORT=4000
NODE_ENV='dev'
BACKEND_URL='http://localhost:4000'
```

## Base de Données
Créer une base de données nommées PossessionsList. Sequelize s'occupera de créer automatiquement les tables. Il peut être nécessaire de devoir adapter le fichier config.js (/config/config.js) car fait pour PostgreSQL.

## Lancement
Taper `cd API` pour se rendre dans le dossier API.
Puis taper `npm install` pour installer les node_modules.
Taper `node index.js` pour démarrer l'API.

## Champs de la Base de Données
Au premier lancement de l'API, un utilisateur (admin et user), des catégories et des produits sont automatiquement créés. Toutefois, les produits n'ont pas d'image. Il est possible de la modifier depuis le Frontend.

## Attention
Il est possible que le back et le front se connectent sur le même port, dans ce cas il faudra modifier la configuration pour qu'ils aient un port différent.